package revchess.apps.com.revchess;
import java.util.ArrayList;

public class Checker {
	
	public Checker(){
		super();
	}
	
/*public static void main(String[] args) {
		int[][] board_state= new int[8][8];
		Checker checker=new Checker();
		board_state[0][0]=300;
		board_state[1][1]=500;
		board_state[2][3]=600;
		board_state[3][0]=200;
		int x=checker.check_game_state(board_state,4,4);
		System.out.println(x);
	}*/

public int check_game_state(int[][] board_state, int m, int n){
	//int[] pieces= {600,500,400,300,200,100};
	ArrayList<Integer> pieces=new ArrayList<Integer>();
	pieces.add(600);
	pieces.add(500);
	pieces.add(400);
	pieces.add(300);
	pieces.add(200);
	for(int i=0;i<m;i++){
		for(int j=0;j<n;j++){
					if(pieces.contains(board_state[i][j])){
						switch(board_state[i][j]){
						case 200:
							if(i-1>=0 && j-1>=0 &&pieces.contains(board_state[i-1][j-1])){
								//System.out.println("something in pathking1");
								return 0;
							}
							if(i-1>=0 &&pieces.contains(board_state[i-1][j])){
								//System.out.println("something in pathking2");
								return 0;
							}
							if(i-1>=0 && j+1<=n &&pieces.contains(board_state[i-1][j+1])){
								//System.out.println("something in pathking3");
								return 0;
							}
							if( j-1>=0 &&pieces.contains(board_state[i][j-1])){
								//System.out.println("something in pathking4");
								return 0;
							}
							if(j+1<=n &&pieces.contains(board_state[i][j+1])){
								//System.out.println("something in pathking5");
								return 0;
							}
							if(i+1<=m && j-1>=0 &&pieces.contains(board_state[i+1][j-1])){
								//System.out.println("something in pathking6");
								return 0;
							}
							if(i+1<=m &&pieces.contains(board_state[i+1][j])){
								//System.out.println("something in pathking7");
								return 0;
							}
							if(i+1>=0 && j+1<=n &&pieces.contains(board_state[i+1][j+1])){
								//System.out.println("something in pathking8");
								return 0;
							}
							break;
						case 300:
							if(i-2>=0 &&  j+1<n && pieces.contains(board_state[i-2][j+1]) ){
								//System.out.println("something in pathknight1");
								return 0;
							}
							if(i-2>=0 && j-1>=0 && pieces.contains(board_state[i-2][j-1]) ){
								//System.out.println("something in pathknight2");
								return 0;
							}							
							if(i+2<=m && j+1<n && pieces.contains(board_state[i+2][j+1]) ){
								//System.out.println("something in pathknight3");
								return 0;
							}
							if(i+2<=m && j-1>=0 && pieces.contains(board_state[i+2][j-1])){
								//System.out.println("something in pathknight4");
								return 0;
							}
							if(i-1>=0 && j-2>=0 && pieces.contains(board_state[i-1][j-2]) ){
								//System.out.println("something in pathknight5");
								return 0;
							}
							if( i-1>=0 && j+2<n && pieces.contains(board_state[i-1][j+2])){
								//System.out.println("something in pathknight6");
								return 0;
							}
							if(i+1<=m && j+2<n && pieces.contains(board_state[i+1][j+2]) ){
								//System.out.println("something in pathknight7");
								return 0;
							}
							if( i+1<m && j-2>=0 && pieces.contains(board_state[i+1][j-2])){
								//System.out.println("something in pathknight8");
								return 0;
							}
							
							break;
						case 400:
							int sum=i+j;
							System.out.println("sum is:"+sum);
							for(int k=0;k<m;k++){
								for(int l=0;l<n;l++){
									if(sum==0){
									if(k==l && k!=i && l!=j && pieces.contains(board_state[k][l])){
										System.out.println("something is in pathbishop0"+board_state[k][l]);
										//board_state[k][l]+=1;
										return 0;
									}
									}
									
									if(k-1>=0 &&  l-1>=0 /*&& k!=i && l!=j*/ && /*pieces.contains(board_state[k][l])*/ board_state[k][l]==400 && pieces.contains(board_state[k-1][l-1])){
									
										System.out.println("something is in pathbishop1"+board_state[k][l]+(k-1)+(l-1)+k+l);
										//board_state[k][l]+=1;
										return 0;
									}
									if(k+1<=m && l+1<=n /*&& k!=i && l!=j*/ && /*pieces.contains(board_state[k][l])*/ board_state[k][l]==400 && pieces.contains(board_state[k+1][l+1])){
										System.out.println("something is in pathbishop2"+board_state[k][l]+(k+1)+(l+1));
										//board_state[k][l]+=1;
										return 0;
									}
									if(k+l==sum && k!=i && l!=j && pieces.contains(board_state[k][l])){
										System.out.println("something is in pathbishop3"+board_state[k][j]+k+j);
										//board_state[k][l]+=1;
										return 0;
									}
								}
								
							}
							break;
						case 500:
							for(int k=0;k<m;k++){
								if(pieces.contains(board_state[i][k]) && k!=j){
									//System.out.println("something is in pathrook1");
									return 0;
									
								}
								if(pieces.contains(board_state[k][j]) && k!=i){
									//System.out.println("something is in pathrook2");
									return 0;
								}
							}
							break;
						case 600:
							sum=i+j;
							System.out.println("the sum is:"+sum);
							for(int k=0;k<m;k++){
								if(pieces.contains(board_state[k][j]) && k!=i){
									System.out.println("something is in pathqueen2");
									//board_state[k][j]+=1;
									return 0;
								}
								for(int l=0;l<n;l++){
									if(pieces.contains(board_state[i][l]) && l!=j){
										System.out.println("something is in pathqueen1");
										//board_state[i][k]+=1;
										return 0;
									}
									if(sum==0){
									if(k==l && k!=i && l!=j && pieces.contains(board_state[k][l])){
										System.out.println("something is in pathqueen4"+board_state[k][l]+k+l);
										//board_state[k][l]+=1;
										return 0;
									}
									}
									if(k-1>=0 && l-1>=0 && /*pieces.contains(board_state[k][l])*/ board_state[k][l]==600&& pieces.contains(board_state[k-1][l-1])){
										System.out.println("something is in pathqueen51"+board_state[k][l]+k+l);
										//board_state[k][l]+=1;
										return 0;
									}
									if(k+1<=m && l+1<=n && /*pieces.contains(board_state[k][l])*/ board_state[k][l]==600 && pieces.contains(board_state[k+1][l+1])){
										System.out.println("something is in pathqueen6"+board_state[k][l]+k+l+(k+1)+(l+1));
										//board_state[k][l]+=1;
										return 0;
									}
									if(k+l==sum && pieces.contains(board_state[k][l]) && k!=i){
										System.out.println("something is in pathqueen3");
										//board_state[k][l]+=1;
										return 0;
									}
								}
							}
							break;
						}
				}
			}
		}
	return 1; //all moves are successful
	//return 0 for unsuccessful
}

}
