package revchess.apps.com.revchess;

/**
 * Created by ameyruikar on 9/15/15.
 */

import android.content.Context;
import android.widget.*;
import android.view.*;

import java.util.ArrayList;
import java.util.*;

public class ImageAdapter extends BaseAdapter {

    private Context mContext;
    private int m,n;
    private List<Integer> props;
    private int ht, width;


    public ImageAdapter(Context c, int m_size, int n_size,ArrayList<Integer> property) {
        mContext = c;
        int index = 0;
        props=new ArrayList<Integer>(property);
        m= m_size;
        n = n_size;



    }

    public int getCount() {
        return props.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        ImageView imageView;

        if(convertView == null){

            imageView = new ImageView(mContext);
/*
                    imageView.setLayoutParams(new GridView.LayoutParams(width, ht));
                    Log.i("width2",String.valueOf(imageView.getLayoutParams().width));
 *
 */
            imageView.setScaleType(ImageView.ScaleType.CENTER);
            imageView.setPadding(0, 0, 0, 0);


        }else{

            imageView = (ImageView)convertView;
        }


        Integer[] temp = props.toArray(new Integer[props.size()]);

        imageView.setImageResource(temp[position]);

        return imageView;
    }





}
