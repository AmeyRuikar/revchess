package revchess.apps.com.revchess;


import android.content.Context;
import android.widget.*;
import android.view.*;

import java.util.ArrayList;
import java.util.*;

/**
 * Created by ameyruikar on 9/13/15.
 */
public class ImageAdapterG2 extends BaseAdapter {

    private Context mContext;
    private int m,n;
    private List<Integer> props;
    private int ht, width;


    public ImageAdapterG2(Context c,  ArrayList<Integer> pieceList) {

        mContext = c;


        n = pieceList.size();

        props = new ArrayList<Integer>();



        for(int i=0; i < n; i++){


            switch(pieceList.get(i)){


                case 200:
                    props.add(R.drawable.k200);

                    break;

                case 300:
                    props.add(R.drawable.h300);

                    break;

                case 400:
                    props.add(R.drawable.b400);

                    break;

                case 500:
                    props.add(R.drawable.r500);
                    break;

                case 600:
                    props.add(R.drawable.q600);
                    break;

            }




        }
    }

    public int getCount() {
        return props.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        ImageView imageView;

        if(convertView == null){

            imageView = new ImageView(mContext);
/*
                    imageView.setLayoutParams(new GridView.LayoutParams(width, ht));
                    Log.i("width2",String.valueOf(imageView.getLayoutParams().width));
 *
 */
            imageView.setScaleType(ImageView.ScaleType.CENTER);
            imageView.setPadding(6, 6, 6, 6);


        }else{

            imageView = (ImageView)convertView;
        }


        Integer[] temp = props.toArray(new Integer[props.size()]);

        imageView.setImageResource(temp[position]);

        return imageView;
    }





}
