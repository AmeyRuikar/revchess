package revchess.apps.com.revchess;


        import android.app.AlertDialog;
        import android.content.Context;
        import android.content.DialogInterface;
        import android.content.Intent;
        import android.content.SharedPreferences;
        import android.location.Address;
        import android.location.Geocoder;
        import android.support.v7.app.AppCompatActivity;
        import android.os.Bundle;
        import android.text.InputType;
        import android.view.Menu;
        import android.view.MenuItem;
        import android.view.ViewGroup;
        import android.widget.GridView;
        import android.widget.AdapterView;
        import android.view.View;
        import android.widget.*;

        import android.util.*;

        import java.util.ArrayList;
        import java.util.List;
        import java.util.Locale;


public class MainActivity extends AppCompatActivity {


    Context mContext;
    GridView grid, grid2;
    ArrayList<Integer> pieces;
    ArrayList<Integer> boardList;
    private boolean setToBoard;
    private boolean boardSet;
    int noOfPieces;
    int current;
    int boardPosition;
    int setPosition;
    int m;
    int n;
    int LeveL;
    int finalAlert;
    level [] gameLevels;

    SharedPreferences p;
    int [][] boardState = new int[10][10];
    @Override
    public void onBackPressed()
    {

        // super.onBackPressed(); // Comment this super call to avoid calling finish()
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mContext = this;
        finalAlert = 0;

        //***shared pref

    p = this.getSharedPreferences("revchess.apps.com.revchess", Context.MODE_PRIVATE );

        int sp = p.getInt("levelNUmber", -1);

        Log.i("sp",String.valueOf(sp));

        if(sp!= -1){

            LeveL = sp;


            if(LeveL == 51){

                LeveL =1;
                finalAlert =1;
                p.edit().putInt("levelNUmber", 1).apply();
            }
            Log.i("LEVEL",String.valueOf(LeveL));
        }
        else{


            LeveL = 1;
            p.edit().putInt("levelNUmber", 1).apply();
            //p.edit().apply();
        }



        if(finalAlert == 1){

            Log.i("inside IF", "1 ");

            final AlertDialog.Builder builder = new AlertDialog.Builder(mContext);

            builder.setTitle("RevChess");
            builder.setMessage("Congrats Grand Master, stay tuned for more levels!!");


            Log.i("inside IF", "2 ");
            builder.setPositiveButton("Ok!", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    p.edit().putInt("levelNUmber", 1).apply();

                    Intent intent = new Intent(mContext, MainActivity.class);
                    startActivity(intent);

                    dialog.dismiss();


                }
            });

            Log.i("inside IF", "3 ");

            builder.show();

            finalAlert = 0;

        }


        //end
        //change string at the top

        TextView tv = (TextView) findViewById(R.id.textView);

        tv.setText("                   Level " + String.valueOf(LeveL));


        //end of changes
        //***********************initializing the game levels***********************************
        gameLevels = new level[52];

        ArrayList<Integer> tempPieces = new ArrayList<Integer>();

        //starting with the first game
        tempPieces.add(400);
        tempPieces.add(400);
        gameLevels[1] = new level(2,2, tempPieces);
        tempPieces.clear();

        tempPieces.add(500);
        tempPieces.add(500);
        tempPieces.add(500);
        gameLevels[2] = new level(3,3, tempPieces);
        tempPieces.clear();

        tempPieces.add(400);
        tempPieces.add(400);
        tempPieces.add(400);
        tempPieces.add(400);
        gameLevels[3] = new level(3,3, tempPieces);
        tempPieces.clear();

        tempPieces.add(300);
        tempPieces.add(300);
        tempPieces.add(300);
        tempPieces.add(300);
        tempPieces.add(300);
        tempPieces.add(300);
        gameLevels[4] = new level(4,4, tempPieces);
        tempPieces.clear();

        tempPieces.add(200);
        tempPieces.add(400);
        tempPieces.add(600);
        gameLevels[5] = new level(3,4, tempPieces);
        tempPieces.clear();

        tempPieces.add(200);
        tempPieces.add(300);
        tempPieces.add(300);
        gameLevels[6] = new level(3,4, tempPieces);
        tempPieces.clear();

        tempPieces.add(200);
        tempPieces.add(500);
        tempPieces.add(500);
        gameLevels[7] = new level(3,3, tempPieces);
        tempPieces.clear();

        tempPieces.add(300);
        tempPieces.add(300);
        tempPieces.add(300);
        tempPieces.add(300);
        tempPieces.add(300);
        gameLevels[8] = new level(3,3, tempPieces);
        tempPieces.clear();


        tempPieces.add(200);
        tempPieces.add(300);
        tempPieces.add(500);
        tempPieces.add(600);
        gameLevels[9] = new level(4,4, tempPieces);
        tempPieces.clear();


        tempPieces.add(200);
        tempPieces.add(300);
        tempPieces.add(600);
        gameLevels[10] = new level(4,4, tempPieces);
        tempPieces.clear();

        tempPieces.add(400);
        tempPieces.add(400);
        tempPieces.add(300);
        tempPieces.add(300);
        gameLevels[11] = new level(4,3, tempPieces);
        tempPieces.clear();


        tempPieces.add(400);
        tempPieces.add(400);
        tempPieces.add(300);
        tempPieces.add(200);
        gameLevels[12] = new level(4,3, tempPieces);
        tempPieces.clear();

        tempPieces.add(500);
        tempPieces.add(500);
        tempPieces.add(400);
        tempPieces.add(200);
        gameLevels[13] = new level(4,4, tempPieces);
        tempPieces.clear();

        tempPieces.add(400);
        tempPieces.add(400);
        tempPieces.add(400);
        tempPieces.add(500);
        tempPieces.add(200);
        gameLevels[14] = new level(4,4, tempPieces);
        tempPieces.clear();


        tempPieces.add(600);
        tempPieces.add(300);
        tempPieces.add(500);
        tempPieces.add(300);
        tempPieces.add(300);
        tempPieces.add(200);
        gameLevels[15] = new level(6,6, tempPieces);
        tempPieces.clear();


        tempPieces.add(400);
        tempPieces.add(400);
        tempPieces.add(400);
        tempPieces.add(600);
        tempPieces.add(200);
        gameLevels[16] = new level(4,4, tempPieces);
        tempPieces.clear();


        tempPieces.add(400);
        tempPieces.add(400);
        tempPieces.add(300);
        tempPieces.add(300);
        tempPieces.add(200);
        gameLevels[17] = new level(3,4, tempPieces);
        tempPieces.clear();


        tempPieces.add(600);
        tempPieces.add(600);
        tempPieces.add(600);
        tempPieces.add(600);
        tempPieces.add(600);
        tempPieces.add(600);
        tempPieces.add(600);
        tempPieces.add(600);
        gameLevels[18] = new level(8,8, tempPieces);
        tempPieces.clear();



        tempPieces.add(600);
        tempPieces.add(600);
        tempPieces.add(600);
        tempPieces.add(500);
        tempPieces.add(500);
        tempPieces.add(200);
        tempPieces.add(200);
        gameLevels[19] = new level(7,7, tempPieces);
        tempPieces.clear();


        tempPieces.add(400);
        tempPieces.add(400);
        tempPieces.add(300);
        tempPieces.add(300);
        tempPieces.add(300);
        tempPieces.add(200);
        tempPieces.add(200);
        gameLevels[20] = new level(5,5 , tempPieces);
        tempPieces.clear();



        tempPieces.add(400);
        tempPieces.add(300);
        tempPieces.add(300);
        tempPieces.add(300);
        tempPieces.add(200);
        tempPieces.add(200);
        tempPieces.add(200);
        gameLevels[21] = new level(6,5 , tempPieces);
        tempPieces.clear();


        tempPieces.add(500);
        tempPieces.add(300);
        tempPieces.add(300);
        tempPieces.add(300);
        tempPieces.add(400);
        tempPieces.add(400);
        tempPieces.add(200);
        gameLevels[22] = new level(6,5 , tempPieces);
        tempPieces.clear();


        tempPieces.add(500);
        tempPieces.add(600);
        tempPieces.add(600);
        tempPieces.add(600);
        tempPieces.add(600);
        tempPieces.add(600);
        gameLevels[23] = new level(6,6 , tempPieces);
        tempPieces.clear();


        tempPieces.add(600);
        tempPieces.add(500);
        tempPieces.add(400);
        tempPieces.add(400);
        tempPieces.add(400);
        tempPieces.add(200);
        tempPieces.add(200);
        gameLevels[24] = new level(6,7 , tempPieces);
        tempPieces.clear();



        tempPieces.add(500);
        tempPieces.add(400);
        tempPieces.add(300);
        tempPieces.add(200);
        tempPieces.add(200);
        tempPieces.add(200);
        gameLevels[25] = new level(6,5 , tempPieces);
        tempPieces.clear();


        tempPieces.add(500);
        tempPieces.add(400);
        tempPieces.add(300);
        tempPieces.add(300);
        tempPieces.add(200);
        tempPieces.add(200);
        gameLevels[26] = new level(5,7 , tempPieces);
        tempPieces.clear();


        tempPieces.add(200);
        tempPieces.add(300);
        tempPieces.add(400);
        tempPieces.add(400);
        tempPieces.add(400);
        tempPieces.add(400);
        tempPieces.add(300);
        gameLevels[27] = new level(6,7 , tempPieces);
        tempPieces.clear();


        tempPieces.add(200);
        tempPieces.add(200);
        tempPieces.add(200);
        tempPieces.add(200);
        tempPieces.add(400);
        tempPieces.add(400);
        tempPieces.add(300);
        tempPieces.add(600);
        gameLevels[28] = new level(5,5 , tempPieces);
        tempPieces.clear();


        tempPieces.add(200);
        tempPieces.add(200);
        tempPieces.add(400);
        tempPieces.add(400);
        tempPieces.add(400);
        tempPieces.add(400);
        tempPieces.add(300);
        gameLevels[29] = new level(5,5 , tempPieces);
        tempPieces.clear();



        tempPieces.add(200);
        tempPieces.add(400);
        tempPieces.add(400);
        tempPieces.add(300);
        tempPieces.add(300);
        tempPieces.add(600);
        gameLevels[30] = new level(5,5 , tempPieces);
        tempPieces.clear();


        tempPieces.add(200);
        tempPieces.add(400);
        tempPieces.add(500);
        tempPieces.add(500);
        gameLevels[31] = new level(4,4 , tempPieces);
        tempPieces.clear();

        tempPieces.add(200);
        tempPieces.add(200);
        tempPieces.add(200);
        tempPieces.add(400);
        tempPieces.add(400);
        gameLevels[32] = new level(4,4 , tempPieces);
        tempPieces.clear();

        tempPieces.add(200);
        tempPieces.add(200);
        tempPieces.add(300);
        tempPieces.add(400);
        tempPieces.add(400);
        tempPieces.add(500);
        gameLevels[33] = new level(4,8 , tempPieces);
        tempPieces.clear();

        tempPieces.add(200);
        tempPieces.add(200);
        tempPieces.add(600);
        tempPieces.add(400);
        tempPieces.add(400);
        tempPieces.add(500);
        gameLevels[34] = new level(5,8 , tempPieces);
        tempPieces.clear();

        tempPieces.add(200);
        tempPieces.add(200);
        tempPieces.add(300);
        tempPieces.add(300);
        tempPieces.add(300);
        tempPieces.add(500);
        tempPieces.add(500);
        tempPieces.add(400);
        tempPieces.add(600);
        gameLevels[35] = new level(8,8 , tempPieces);
        tempPieces.clear();


        tempPieces.add(300);
        tempPieces.add(400);
        tempPieces.add(400);
        tempPieces.add(600);
        gameLevels[36] = new level(3,4 , tempPieces);
        tempPieces.clear();



        tempPieces.add(200);
        tempPieces.add(300);
        tempPieces.add(300);
        tempPieces.add(300);
        tempPieces.add(400);
        tempPieces.add(400);
        tempPieces.add(600);
        gameLevels[37] = new level(6,8 , tempPieces);
        tempPieces.clear();

        tempPieces.add(200);
        tempPieces.add(300);
        tempPieces.add(400);
        tempPieces.add(400);
        gameLevels[38] = new level(4,4 , tempPieces);
        tempPieces.clear();

        tempPieces.add(200);
        tempPieces.add(200);
        tempPieces.add(200);
        tempPieces.add(400);
        tempPieces.add(400);
        tempPieces.add(500);
        gameLevels[39] = new level(5,5 , tempPieces);
        tempPieces.clear();

        tempPieces.add(400);
        tempPieces.add(400);
        tempPieces.add(400);
        tempPieces.add(500);
        tempPieces.add(500);
        gameLevels[40] = new level(4,4 , tempPieces);
        tempPieces.clear();


        tempPieces.add(200);
        tempPieces.add(200);
        tempPieces.add(300);
        tempPieces.add(300);
        tempPieces.add(300);
        tempPieces.add(300);
        gameLevels[41] = new level(4,4 , tempPieces);
        tempPieces.clear();


        tempPieces.add(600);
        tempPieces.add(600);
        tempPieces.add(400);
        tempPieces.add(200);
        tempPieces.add(200);
        tempPieces.add(200);
        tempPieces.add(200);
        gameLevels[42] = new level(6,6 , tempPieces);
        tempPieces.clear();

        tempPieces.add(500);
        tempPieces.add(500);
        tempPieces.add(400);
        tempPieces.add(300);
        tempPieces.add(300);
        tempPieces.add(300);
        tempPieces.add(300);
        tempPieces.add(200);
        gameLevels[43] = new level(6,6 , tempPieces);
        tempPieces.clear();



        tempPieces.add(600);
        tempPieces.add(600);
        tempPieces.add(400);
        tempPieces.add(400);
        tempPieces.add(300);
        tempPieces.add(300);
        tempPieces.add(300);
        tempPieces.add(300);
        tempPieces.add(200);
        gameLevels[44] = new level(7,7 , tempPieces);
        tempPieces.clear();

        tempPieces.add(600);
        tempPieces.add(600);
        tempPieces.add(400);
        tempPieces.add(400);
        tempPieces.add(400);
        tempPieces.add(400);
        tempPieces.add(400);
        tempPieces.add(400);
        tempPieces.add(200);
        gameLevels[45] = new level(7,7 , tempPieces);
        tempPieces.clear();


        tempPieces.add(600);
        tempPieces.add(500);
        tempPieces.add(500);
        tempPieces.add(400);
        tempPieces.add(400);
        tempPieces.add(300);
        tempPieces.add(300);
        tempPieces.add(200);
        tempPieces.add(200);
        gameLevels[46] = new level(7,7 , tempPieces);
        tempPieces.clear();

        tempPieces.add(500);
        tempPieces.add(500);
        tempPieces.add(300);
        tempPieces.add(300);
        tempPieces.add(200);
        tempPieces.add(200);
        tempPieces.add(200);
        tempPieces.add(200);
        gameLevels[47] = new level(7,7 , tempPieces);
        tempPieces.clear();


        tempPieces.add(600);
        tempPieces.add(600);
        tempPieces.add(600);
        tempPieces.add(400);
        tempPieces.add(400);
        tempPieces.add(400);
        tempPieces.add(400);
        tempPieces.add(300);
        tempPieces.add(300);
        gameLevels[48] = new level(8,8 , tempPieces);
        tempPieces.clear();

        tempPieces.add(600);
        tempPieces.add(400);
        tempPieces.add(400);
        tempPieces.add(300);
        tempPieces.add(300);
        tempPieces.add(300);
        tempPieces.add(200);
        tempPieces.add(200);
        tempPieces.add(200);
        gameLevels[49] = new level(8,8 , tempPieces);
        tempPieces.clear();

        tempPieces.add(600);
        tempPieces.add(600);
        tempPieces.add(600);
        tempPieces.add(600);
        tempPieces.add(500);
        tempPieces.add(500);
        tempPieces.add(400);
        tempPieces.add(400);
        tempPieces.add(200);
        tempPieces.add(200);
        gameLevels[50] = new level(8,8 , tempPieces);
        tempPieces.clear();



        //end of initializations****************************************************************


        //Log.i("level", String.valueOf(LeveL));

        n= gameLevels[LeveL].getN();
        m = gameLevels[LeveL].getM();

        mContext = this;
        setToBoard = false;
        boardSet=false;

        boardList=new ArrayList<Integer>();
        for(int i=0;i<Math.min(m, n);i++){
            for(int j=0;j<Math.max(m,n);j++){
                if((i+j)%2==0){
                    boardList.add(R.drawable.brown);
                }
                else{
                    boardList.add(R.drawable.white);
                }
            }
        }


        pieces = new ArrayList<Integer>(gameLevels[LeveL].getPieces());



        //pieces = ;

        noOfPieces = pieces.size();

        //retrieve Level.


        grid = (GridView) findViewById(R.id.gridView);
        grid.setNumColumns(Math.max(m, n));


        grid2 = (GridView) findViewById(R.id.gridView2);
        grid2.setNumColumns(noOfPieces);
        grid2.setAdapter(new ImageAdapterG2(this,  pieces ));

        ViewGroup.LayoutParams  vg= grid.getLayoutParams();

        float scale  = mContext.getResources().getDisplayMetrics().density;

        vg.height = (int) ( (51.5 * Math.min(m,n) * scale) + 0.5);
        vg.width = (int) ( (51.5 * Math.max(m, n) * scale) + 0.5);
        Log.i("HT,WIDTH before", String.valueOf(grid.getLayoutParams().height) + "-"+ String.valueOf(grid.getLayoutParams().width));

        grid.setLayoutParams(vg);
        Log.i("HT,WIDTH after", String.valueOf(grid.getLayoutParams().height) + "-" + String.valueOf(grid.getLayoutParams().width));

        grid.setAdapter(new ImageAdapter(this, Math.max(m,n), Math.min(m,n),boardList));


        //grid.setColumnWidth(this.getResources().getDisplayMetrics().widthPixels / n);

        grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                //Toast.makeText(mContext, "click at" + position, Toast.LENGTH_SHORT).show();
                //Log.i("Width", String.valueOf(grid.getColumnWidth()));
                if (setToBoard == true && (boardList.get(position) == R.drawable.white || boardList.get(position) == R.drawable.brown)) {

                    if (boardList.get(position) == R.drawable.brown) {
                        switch (current) {
                            case 200:
                                boardList.set(position, R.drawable.bk);
                                break;
                            case 300:
                                boardList.set(position, R.drawable.bh);
                                break;
                            case 400:
                                boardList.set(position, R.drawable.bb);
                                break;
                            case 500:
                                boardList.set(position, R.drawable.br);
                                break;
                            case 600:
                                boardList.set(position, R.drawable.bq);
                                break;

                        }
                    } else if (boardList.get(position) == R.drawable.white) {
                        switch (current) {
                            case 200:
                                boardList.set(position, R.drawable.wk);
                                break;
                            case 300:
                                boardList.set(position, R.drawable.wh);
                                break;
                            case 400:
                                boardList.set(position, R.drawable.wb);
                                break;
                            case 500:
                                boardList.set(position, R.drawable.wr);
                                break;
                            case 600:
                                boardList.set(position, R.drawable.wq);
                                break;

                        }
                    }

                    pieces.remove(setPosition);
                    grid2.setAdapter(new ImageAdapterG2(mContext, pieces));
                    grid.setAdapter(new ImageAdapter(mContext, Math.max(m, n), Math.min(m, n), boardList));
                    setToBoard = false;
                    boardSet = false;
                    //**************checker******************

                    if(pieces.size() == 0){

                        Checker ck = new Checker();
                        int i, j,k=Math.max(m,n);

                        for(int x = 0; x< boardList.size(); x++){

                            i = x / k;
                            j = x % k;

                            if(boardList.get(x) == R.drawable.brown ||boardList.get(x) == R.drawable.white){

                                boardState[i][j] = 0;

                            }
                            else if(boardList.get(x) == R.drawable.bk ||boardList.get(x) == R.drawable.wk){

                                boardState[i][j] = 200;

                            }
                            else if(boardList.get(x) == R.drawable.bh ||boardList.get(x) == R.drawable.wh){

                                boardState[i][j] = 300;

                            }
                            else if(boardList.get(x) == R.drawable.bb ||boardList.get(x) == R.drawable.wb){

                                boardState[i][j] = 400;

                            }
                            else if(boardList.get(x) == R.drawable.br ||boardList.get(x) == R.drawable.wr){

                                boardState[i][j] = 500;

                            }
                            else if(boardList.get(x) == R.drawable.bq ||boardList.get(x) == R.drawable.wq){

                                boardState[i][j] = 600;

                            }




                        }


                        int status;

                        status = ck.check_game_state(boardState,Math.min(m,n),Math.max(m,n));



                        if(status == 1){
                            Toast.makeText(mContext, "Correct!! next round", Toast.LENGTH_SHORT).show();
                            p.edit().putInt("levelNUmber", (LeveL + 1)).apply();
                            //p.edit().apply();

                            //store level in shared preference
                            Intent intent = new Intent(mContext, MainActivity.class);
                            startActivity(intent);

                        }
                        else{
                            //Toast.makeText(mContext, "you lose " , Toast.LENGTH_SHORT).show();

                        }

                    }



                    //endOfChecker****************************

                } else if (boardSet == false && setToBoard == false && boardList.get(position) != R.drawable.brown && boardList.get(position) != R.drawable.white) {
                    boardSet = true;
                    boardPosition = position;
                    current = boardList.get(position);
                } else if (boardSet == true && (boardList.get(position) == R.drawable.white || boardList.get(position) == R.drawable.brown)) {
                    if (boardList.get(position) == R.drawable.brown) {
                        if (current == R.drawable.bk || current == R.drawable.wk) {
                            boardList.set(position, R.drawable.bk);
                        }
                        if (current == R.drawable.bh || current == R.drawable.wh) {
                            boardList.set(position, R.drawable.bh);
                        }
                        if (current == R.drawable.bb || current == R.drawable.wb) {
                            boardList.set(position, R.drawable.bb);
                        }
                        if (current == R.drawable.br || current == R.drawable.wr) {
                            boardList.set(position, R.drawable.br);
                        }
                        if (current == R.drawable.bq || current == R.drawable.wq) {
                            boardList.set(position, R.drawable.bq);
                        }
                    } else {
                        if (current == R.drawable.bk || current == R.drawable.wk) {
                            boardList.set(position, R.drawable.wk);
                        }
                        if (current == R.drawable.bh || current == R.drawable.wh) {
                            boardList.set(position, R.drawable.wh);
                        }
                        if (current == R.drawable.bb || current == R.drawable.wb) {
                            boardList.set(position, R.drawable.wb);
                        }
                        if (current == R.drawable.br || current == R.drawable.wr) {
                            boardList.set(position, R.drawable.wr);
                        }
                        if (current == R.drawable.bq || current == R.drawable.wq) {
                            boardList.set(position, R.drawable.wq);
                        }
                    }
                    if (current == R.drawable.wk || current == R.drawable.wh || current == R.drawable.wb || current == R.drawable.wr || current == R.drawable.wq) {
                        boardList.set(boardPosition, R.drawable.white);
                    } else {
                        boardList.set(boardPosition, R.drawable.brown);
                    }
                    grid.setAdapter(new ImageAdapter(mContext, Math.max(m, n), Math.min(m, n), boardList));
                    boardSet = false;

                    //**************checker******************

                    if(pieces.size() == 0){

                        Checker ck = new Checker();
                        int i, j,k=Math.max(m,n);

                        for(int x = 0; x< boardList.size(); x++){

                            i = x / k;
                            j = x % k;

                            if(boardList.get(x) == R.drawable.brown ||boardList.get(x) == R.drawable.white){

                                boardState[i][j] = 0;

                            }
                            else if(boardList.get(x) == R.drawable.bk ||boardList.get(x) == R.drawable.wk){

                                boardState[i][j] = 200;

                            }
                            else if(boardList.get(x) == R.drawable.bh ||boardList.get(x) == R.drawable.wh){

                                boardState[i][j] = 300;

                            }
                            else if(boardList.get(x) == R.drawable.bb ||boardList.get(x) == R.drawable.wb){

                                boardState[i][j] = 400;

                            }
                            else if(boardList.get(x) == R.drawable.br ||boardList.get(x) == R.drawable.wr){

                                boardState[i][j] = 500;

                            }
                            else if(boardList.get(x) == R.drawable.bq ||boardList.get(x) == R.drawable.wq){

                                boardState[i][j] = 600;

                            }




                        }


                        int status;

                        status = ck.check_game_state(boardState,Math.min(m,n),Math.max(m,n));



                        if(status == 1){
                            Toast.makeText(mContext, "you win next round ", Toast.LENGTH_SHORT).show();
                            p.edit().putInt("levelNUmber", (LeveL + 1)).apply();
                            //p.edit().apply();

                            //store level in shared preference
                            Intent intent = new Intent(mContext, MainActivity.class);
                            startActivity(intent);


                        }
                        else{
                            //Toast.makeText(mContext, "you lose " , Toast.LENGTH_SHORT).show();

                        }

                    }



                    //endOfChecker****************************

                }
                else if(boardSet==true && boardPosition==position){
                    if(current==R.drawable.wk || current==R.drawable.wh || current==R.drawable.wb || current==R.drawable.wr || current==R.drawable.wq){
                        boardList.set(boardPosition,R.drawable.white);
                    }
                    else{
                        boardList.set(boardPosition,R.drawable.brown);
                    }

                    if(current==R.drawable.bk || current==R.drawable.wk){
                        pieces.add(200);
                    }
                    if(current==R.drawable.bh || current==R.drawable.wh){
                        pieces.add(300);
                    }
                    if(current==R.drawable.bb || current==R.drawable.wb){
                        pieces.add(400);
                    }
                    if(current==R.drawable.br || current==R.drawable.wr){
                        pieces.add(500);
                    }
                    if(current==R.drawable.bq || current==R.drawable.wq){
                        pieces.add(600);
                    }

                    grid2.setAdapter(new ImageAdapterG2(mContext, pieces));
                    grid.setAdapter(new ImageAdapter(mContext, Math.max(m,n), Math.min(m,n), boardList));
                    setToBoard=false;
                    boardSet=false;
                }
            }
        });


        grid2.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //Toast.makeText(mContext, "picked " + position, Toast.LENGTH_SHORT).show();
                if(boardSet==false && position < pieces.size()){
                    setToBoard = true;

                    current = pieces.get(position);
                    setPosition=position;
                }
                else if(boardSet==true){
                    if(current==R.drawable.wk || current==R.drawable.wh || current==R.drawable.wb || current==R.drawable.wr || current==R.drawable.wq){
                        boardList.set(boardPosition,R.drawable.white);
                    }
                    else{
                        boardList.set(boardPosition,R.drawable.brown);
                    }

                    if(current==R.drawable.bk || current==R.drawable.wk){
                        pieces.add(200);
                    }
                    if(current==R.drawable.bh || current==R.drawable.wh){
                        pieces.add(300);
                    }
                    if(current==R.drawable.bb || current==R.drawable.wb){
                        pieces.add(400);
                    }
                    if(current==R.drawable.br || current==R.drawable.wr){
                        pieces.add(500);
                    }
                    if(current==R.drawable.bq || current==R.drawable.wq){
                        pieces.add(600);
                    }

                    grid2.setAdapter(new ImageAdapterG2(mContext, pieces));
                    grid.setAdapter(new ImageAdapter(mContext, Math.max(m,n), Math.min(m,n), boardList));
                    setToBoard=false;
                    boardSet=false;
                }



            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {

         p.edit().putInt("levelNUmber", 1).apply();

         Intent intent = new Intent(mContext, MainActivity.class);
         startActivity(intent);

         Toast.makeText(mContext, "Going to level 1", Toast.LENGTH_SHORT).show();

            return true;

        }

        return super.onOptionsItemSelected(item);
    }


}

