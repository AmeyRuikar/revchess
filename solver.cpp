#include<iostream>
#include<list>
#include<algorithm>
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */
#define QUEEN 600
#define ROOK 500
#define BISHOP 400
#define KNIGHT 300
#define KING 200
using namespace std;

double difficulty;

int isPiece(int x)
{
	if(x==QUEEN || x==ROOK || x==BISHOP || x==KNIGHT || x==KING)
		return 1;
	return 0;
}

void copy(int dest[8][8],int source[8][8])
{
	for(int i=0;i<8;i++)
		for(int j=0;j<8;j++)
			dest[i][j]=source[i][j];
}

class Board
{
	public:
		int board[8][8];
		int count[8][8];
		int m,n;
	
	Board(int m,int n)
	{
		this->m=m;
		this->n=n;
		for(int i=0;i<m;i++)
			for(int j=0;j<n;j++)
			{
				board[i][j]=0;
				count[i][j]=0;
			}
	}	
	
	Board(Board &a)
	{
		copy(this->board,a.board);
		copy(this->count,a.count);
		this->m=a.m;
		this->n=a.n;
	}

	Board()
	{
		this->m=8;
		this->n=8;
		for(int i=0;i<m;i++)
			for(int j=0;j<n;j++)
			{
				board[i][j]=0;
				count[i][j]=0;
			}
	}

	void printBoard()
	{
		for(int i=0;i<m;i++)
		{
			for(int j=0;j<n;j++)
			{
				cout<<board[i][j]<<"		";
			}
			cout<<endl;
		}
	}

	void printCount()
	{
		for(int i=0;i<m;i++)
		{
			for(int j=0;j<n;j++)
			{
				cout<<count[i][j]<<"		";
			}
			cout<<endl;
		}
	}

	int insert(int i,int j,int piece)
	{
		if(i > m-1 || i<0 || j>n-1 || j<0 || board[i][j]!=0 || count[i][j]!=0)
			return 0;
		board[i][j]=piece;
		count[i][j]++;
		if(piece==QUEEN)
		{
			for(int x=i+1;x<m;x++)
				count[x][j]++;
			for(int x=i-1;x>=0;x--)
				count[x][j]++;
			for(int y=j+1;y<n;y++)
				count[i][y]++;	
			for(int y=j-1;y>=0;y--)
				count[i][y]++;

			for(int x=i-1,y=j-1 ; x>=0 && y>=0; x--,y--)
			{
				count[x][y]++;
			}
			for(int x=i-1,y=j+1 ; x>=0 && y<n; x--,y++)
			{
				count[x][y]++;
			}
			for(int x=i+1,y=j-1 ; x<m && y>=0; x++,y--)
			{
				count[x][y]++;
			}
			for(int x=i+1,y=j+1 ; x<m && y<n; x++,y++)
			{
				count[x][y]++;
			}
		}
		else if(piece==ROOK)
		{
			for(int x=i+1;x<m;x++)
				count[x][j]++;
			for(int x=i-1;x>=0;x--)
				count[x][j]++;
			for(int y=j+1;y<n;y++)
				count[i][y]++;	
			for(int y=j-1;y>=0;y--)
				count[i][y]++;
		}

		else if(piece==BISHOP)
		{
			for(int x=i-1,y=j-1 ; x>=0 && y>=0; x--,y--)
			{
				count[x][y]++;
			}
			for(int x=i-1,y=j+1 ; x>=0 && y<n; x--,y++)
			{
				count[x][y]++;
			}
			for(int x=i+1,y=j-1 ; x<m && y>=0; x++,y--)
			{
				count[x][y]++;
			}
			for(int x=i+1,y=j+1 ; x<m && y<n; x++,y++)
			{
				count[x][y]++;
			}
		}

		else if(piece==KING)
		{
			if(i-1 >= 0) count[i-1][j]++;
			if(i+1 < m) count[i+1][j]++;
			if(j-1 >=0) count[i][j-1]++;
			if(j+1 < n) count[i][j+1]++;
			
			if(i-1 >= 0 && j-1>=0) count[i-1][j-1]++;
			if(i-1 >= 0 && j+1<n) count[i-1][j+1]++;
			if(i+1 < m && j-1>=0) count[i+1][j-1]++;
			if(i+1 < m && j+1<n) count[i+1][j+1]++;
		}

		else if(piece==KNIGHT)
		{
			if(i-2 >= 0 && j-1 >=0) count[i-2][j-1]++;
			if(i-2 >= 0 && j+1 < n) count[i-2][j+1]++;
			if(i+2 < m && j-1 >= 0) count[i+2][j-1]++;
			if(i+2 <m && j+1 < n) count[i+2][j+1]++;

			if(i-1 >= 0 && j-2 >=0) count[i-1][j-2]++;
			if(i-1 >= 0 && j+2 < n) count[i-1][j+2]++;
			if(i+1 < m && j-2 >= 0) count[i+1][j-2]++;
			if(i+1 <m && j+2 < n) count[i+1][j+2]++;
		}

		return 1;
	}

	int remove(int i,int j)
	{
		if(i > m-1 || i<0 || j>n-1 || j<0 || board[i][j]==0 || count[i][j]==0)
			return 0;
		int piece=board[i][j];

		board[i][j]=0;
		count[i][j]=0;
		
		if(piece==QUEEN)
		{
			for(int x=i+1;x<m;x++)
				count[x][j]--;
			for(int x=i-1;x>=0;x--)
				count[x][j]--;
			for(int y=j+1;y<n;y++)
				count[i][y]--;	
			for(int y=j-1;y>=0;y--)
				count[i][y]--;

			for(int x=i-1,y=j-1 ; x>=0 && y>=0; x--,y--)
			{
				count[x][y]--;
			}
			for(int x=i-1,y=j+1 ; x>=0 && y<n; x--,y++)
			{
				count[x][y]--;
			}
			for(int x=i+1,y=j-1 ; x<m && y>=0; x++,y--)
			{
				count[x][y]--;
			}
			for(int x=i+1,y=j+1 ; x<m && y<n; x++,y++)
			{
				count[x][y]--;
			}
		}
		else if(piece==ROOK)
		{
			for(int x=i+1;x<m;x++)
				count[x][j]--;
			for(int x=i-1;x>=0;x--)
				count[x][j]--;
			for(int y=j+1;y<n;y++)
				count[i][y]--;	
			for(int y=j-1;y>=0;y--)
				count[i][y]--;
		}

		else if(piece==BISHOP)
		{
			for(int x=i-1,y=j-1 ; x>=0 && y>=0; x--,y--)
			{
				count[x][y]--;
			}
			for(int x=i-1,y=j+1 ; x>=0 && y<n; x--,y++)
			{
				count[x][y]--;
			}
			for(int x=i+1,y=j-1 ; x<m && y>=0; x++,y--)
			{
				count[x][y]--;
			}
			for(int x=i+1,y=j+1 ; x<m && y<n; x++,y++)
			{
				count[x][y]--;
			}
		}

		else if(piece==KING)
		{
			if(i-1 >= 0) count[i-1][j]--;
			if(i+1 < m) count[i+1][j]--;
			if(j-1 >=0) count[i][j-1]--;
			if(j+1 < n) count[i][j+1]--;
			
			if(i-1 >= 0 && j-1>=0) count[i-1][j-1]--;
			if(i-1 >= 0 && j+1<n) count[i-1][j+1]--;
			if(i+1 < m && j-1>=0) count[i+1][j-1]--;
			if(i+1 < m && j+1<n) count[i+1][j+1]--;
		}

		else if(piece==KNIGHT)
		{
			if(i-2 >= 0 && j-1 >=0) count[i-2][j-1]--;
			if(i-2 >= 0 && j+1 < n) count[i-2][j+1]--;
			if(i+2 < m && j-1 >= 0) count[i+2][j-1]--;
			if(i+2 <m && j+1 < n) count[i+2][j+1]--;

			if(i-1 >= 0 && j-2 >=0) count[i-1][j-2]--;
			if(i-1 >= 0 && j+2 < n) count[i-1][j+2]--;
			if(i+1 < m && j-2 >= 0) count[i+1][j-2]--;
			if(i+1 <m && j+2 < n) count[i+1][j+2]--;
		}


		return 1;
	}

	int getPiece(int i,int j)
	{
		return board[i][j];
	}

	int check()
	{
		for(int i=0;i<m;i++)
			for(int j=0;j<n;j++)
				if(count[i][j]>1 && isPiece(board[i][j]))
					return 0;
		return 1;
	}

	void copyBoard(Board &b)
	{
		this->m=b.m;
		this->n=b.n;
		for(int i=0;i<m;i++)
		{
			for(int j=0;j<n;j++)
			{
				this->board[i][j]=b.board[i][j];
				this->count[i][j]=b.count[i][j];
			}
		}
	}

};

int fun(list<int> pieceList,int m,int n, Board *b,int old)
{
	if(pieceList.empty())
		return 1;

	difficulty=difficulty + (1.0/(old-pieceList.size() +1));

	//cout<<"Currently length of piecelist is : "<<pieceList.size()<<endl;

	list<int> tempList(pieceList);
	Board b2(*b);
	
	int currentPiece=tempList.back();
	tempList.pop_back();

	for(int i=0;i<m;i++)
	{
		for(int j=0;j<n;j++)
		{
			int status=b2.insert(i,j,currentPiece);
			//cout<<"Status for inserting "<<currentPiece<<" at "<<i<<","<<j<<" is "<<status<<endl;
			if(status==1 && b2.check())
			{
				//cout<<"almost called recursive function"<<endl;
				int status2=fun(tempList,m,n,&b2,old);
				if(status2 == 1)
				{
					b->copyBoard(b2);
					return 1;
				}
				else
				{
					b2.remove(i,j);
				}
			}
		}
	}	
	return 0;			
}

int getPiece()
{
	int x;
	x=rand()%5+2;
	return x*100;
}

int main()
{
		
	/*
	difficulty=0.0;
	list<int> piece;
	
	piece.push_back(KNIGHT);
	piece.push_back(KNIGHT);
	piece.push_back(KNIGHT);
	piece.push_back(KNIGHT);
	piece.push_back(KNIGHT);
	piece.push_back(KNIGHT);
	piece.push_back(KNIGHT);
	piece.push_back(KNIGHT);
	Board b(4,4);
	if(fun(piece,4,4,&b,piece.size()))
	{
		cout<<"Possible! Answer is : "<<endl;
		b.printBoard();
	}
	else
	{
		cout<<"Not possible!"<<endl;
	}
	cout<<"Problem difficulty level is : "<<difficulty<<endl;
	*/	

	srand (time(NULL));
	while(1)
	{
		difficulty=0;
		cout<<endl;
		list<int> pieceList;
		int m=8;
		int n=m;	 
		int pieceNumber=rand()%5+m;

		for(int i=0;i<pieceNumber;i++)
		{
			int piece=getPiece();
			pieceList.push_back(piece);
		}
		Board b(m,n);
		if(fun(pieceList,m,n,&b,pieceList.size()) && difficulty>70000)
		{
			cout<<"Board dimensions m="<<m<<",n="<<n<<endl;
			cout<<"Pieces are : "<<endl;
			pieceList.sort();
			while(!pieceList.empty())
			{
				cout<<pieceList.back()<<",";
				pieceList.pop_back();
			}
			cout<<endl;
			b.printBoard();
			cout<<"Difficulty level is : "<<difficulty<<endl;
			int x;
			cin>>x;
		}
	}
}
